﻿using AltaSoftTesteSaulo.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AltaSoftTesteSaulo
{
    public class Log : ILog
    {
        public IEnumerable<String> ReadLog(string logPath)
        {
            if (!File.Exists(logPath))
                File.Create(logPath);

            return File.ReadLines(logPath);
        }

        public void WriteLog(string logPath, string text)
        {
            if (!File.Exists(logPath))
                File.Create(logPath);

            File.AppendAllText(logPath, text + Environment.NewLine);
        }
    }
}
