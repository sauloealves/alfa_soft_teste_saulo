﻿using AltaSoftTesteSaulo.Interfaces;
using System;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;
using System.Linq;
using System.IO;

namespace AltaSoftTesteSaulo
{
    internal class Program
    {
        private static readonly string namePath = "..\\..\\..\\Files\\names.txt";
        private static readonly string logPath = string.Format("{0}\\log.txt", Directory.GetCurrentDirectory()); //"..\\..\\..\\Files\\log.txt";
        private static readonly string exitCommand = "EXIT";

        static async Task Main(string[] args)
        {
            var serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection);
            var serviceProvider = serviceCollection.BuildServiceProvider();

            var fileService = serviceProvider.GetService<IReadFile>();
            var apiService = serviceProvider.GetService<IAPIService>();
            var logService = serviceProvider.GetService<ILog>();

            // First I have to check if the last execute have more than 60 seconds from now
            var log = logService.ReadLog(logPath);

            if (log.Count() > 0)
            {
                var lastExecute = DateTime.Parse(log.Last().Split(";")[0]);

                if ((DateTime.Now - lastExecute).TotalSeconds < 60)
                {
                    Console.WriteLine("You can't execute this service less 60 seconds from last execute");
                    Console.WriteLine("Press any key to exit");
                    Console.ReadLine();
                    Environment.Exit(0);
                }
            }

            string path = "";

            while (path == "" && path.ToUpper() != exitCommand)
            {
                path = ReadInitialMenu();
                if (!File.Exists(path) && path.ToUpper() != exitCommand)
                {
                    Console.WriteLine("Path not exist");
                    path = "";
                }
            }

            if (path.ToUpper() == exitCommand)
                Environment.Exit(0);

            var names = fileService.ReadFileFromPath(path);

            foreach (var item in names)
            {
                var result = await apiService.SendApi(item);
                string retorno = string.Format("{0}; type: {1} message: {2}", DateTime.Now.ToString(), result.Type, result.Error.Message);
                logService.WriteLog(logPath, retorno);
                Console.WriteLine(retorno);
                Task.Delay(5000).Wait();
            }

        }

        private static string ReadInitialMenu()
        {
            Console.WriteLine("Enter full path from file or 'EXIT' to end: ");
            string opcao = Console.ReadLine();

            return opcao;
        }

        public static void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IAPIService, APIService>()
                .AddScoped<IReadFile, ReadFile>()
                .AddScoped<ILog, Log>();
        }

        
    }
}
