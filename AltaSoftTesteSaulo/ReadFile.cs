﻿using AltaSoftTesteSaulo.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AltaSoftTesteSaulo
{
    public class ReadFile: IReadFile
    {
        public IEnumerable<String> ReadFileFromPath(string pathFile)
        {
            return File.ReadLines(pathFile);           

        }
    }
}
