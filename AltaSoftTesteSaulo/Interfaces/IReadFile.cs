﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AltaSoftTesteSaulo.Interfaces
{
    public interface IReadFile
    {
        public IEnumerable<string> ReadFileFromPath(string path);
    }
}
