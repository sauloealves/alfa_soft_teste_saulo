﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AltaSoftTesteSaulo.Interfaces
{
    public interface ILog
    {
        public void WriteLog(string logPath, string text);

        public IEnumerable<string> ReadLog(string logPath);
    }
}
