﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AltaSoftTesteSaulo.Models
{
    public class APIResponse
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("error")]
        public MessageResponse Error { get; set; }
    }
}
