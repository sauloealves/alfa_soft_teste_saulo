﻿using Newtonsoft.Json;

namespace AltaSoftTesteSaulo.Models
{
    public class MessageResponse
    {
        [JsonProperty("message")]
        public string Message { get; set; }
    }
}