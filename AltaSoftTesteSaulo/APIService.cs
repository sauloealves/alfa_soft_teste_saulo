﻿using AltaSoftTesteSaulo.Interfaces;
using AltaSoftTesteSaulo.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;


namespace AltaSoftTesteSaulo
{
    public class APIService : IAPIService
    {
        public async Task<APIResponse> SendApi(string name)
        {
            var client = new HttpClient();

            Uri uri = new Uri(string.Format("https://api.bitbucket.org/2.0/users/{0}",name));

            var responseMessage = await client.GetAsync(uri);
            var content = await responseMessage.Content.ReadAsStringAsync();
            var retorno = JsonConvert.DeserializeObject<APIResponse>(content);

            return retorno;
        }

    }
}
